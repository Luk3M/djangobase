from django import forms

from .models import Item, Property


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['label', 'description', 'aliases']
        labels = {
            'aliases': "Aliases"
        }
        help_texts = {
            'aliases': "A list of alternative names, separated by pipe character (|)."
        }

class PropertyForm(forms.ModelForm):
    class Meta:
        model = Property
        fields = ['label', 'description', 'value_type', 'aliases']
        labels = {
            'aliases': "Aliases"
        }
        help_texts = {
            'aliases': "A list of alternative names, separated by pipe character (|)."
        }
