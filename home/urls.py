from django.urls import path

from .views import (ClaimCreateView, ClaimUpdateView, EntityDetailView,
                    ItemCreateView, ItemUpdateView, PropertyCreateView,
                    PropertyUpdateView, SpecialPagesView)

app_name = "home"

urlpatterns = [
    path('entities/<slug:slug>/', EntityDetailView.as_view(), name='entity-detail'),
    path('entities/create-claim', ClaimCreateView.as_view(), name='claim-create'),
    path('entities/update-claim/<int:pk>', ClaimUpdateView.as_view(), name='claim-update'),
    path('entities/create-item', ItemCreateView.as_view(), name='item-create'),
    path('entities/update-item/<slug:slug>', ItemUpdateView.as_view(), name='item-update'),
    path('entities/create-property', PropertyCreateView.as_view(), name='property-create'),
    path('entities/update-property/<slug:slug>', PropertyUpdateView.as_view(), name='property-update'),
    path('special-pages', SpecialPagesView.as_view(), name='special-pages'),
]
