from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.edit import (CreateView, DeleteView, FormView,
                                       UpdateView)

from .forms import ItemForm, PropertyForm
from .models import Claim, Entity, Item, Property
from django.views.generic import TemplateView


class EntityDetailView(DetailView):
    model = Entity

class ClaimCreateView(CreateView):
    model = Claim
    fields = ['entity', 'prop', 'value', 'rank']

    def get_initial(self):
        initial = super(ClaimCreateView, self).get_initial()
        initial = initial.copy()
        if self.request.GET.get("slug"):
            initial['entity'] = Entity.objects.get(slug=self.request.GET.get("slug"))
        return initial

class ClaimUpdateView(UpdateView):
    model = Claim
    fields = ['value', 'rank']

class ItemCreateView(CreateView):
    model = Item
    form_class = ItemForm

class ItemUpdateView(UpdateView):
    model = Item
    form_class = ItemForm

class PropertyCreateView(CreateView):
    model = Property
    form_class = PropertyForm

class PropertyUpdateView(UpdateView):
    model = Property
    form_class = PropertyForm

class SpecialPagesView(TemplateView):
    template_name = "home/special_pages.html"
