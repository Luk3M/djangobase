import string
import uuid
from builtins import NotImplementedError, hasattr, property

from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from taggit.managers import TaggableManager

import random
from struct import pack


def random_generator(size=7, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def is_slug_valid(slug):
    if slug.startswith("Q") or slug.startswith("P"):
        return True
    else:
        return False

class Entity(models.Model):
    slug = models.SlugField(max_length=30, blank=True)
    label = models.CharField(max_length=30, blank=True)
    description = models.CharField(max_length=200, blank=True)
    timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)
    aliases = TaggableManager(blank=True)

    def __str__(self):
        return "{}".format(self.slug)

    def get_absolute_url(self):
        return reverse('home:entity-detail', kwargs={'slug': self.slug})
    
    def get_edit_url(self):
        if hasattr(self, 'item'):
            return reverse('home:item-update', kwargs={'slug': self.slug})
        elif hasattr(self, 'property'):
            return reverse('home:property-update', kwargs={'slug': self.slug})
        else:
            raise NotImplementedError

    def get_aliases(self):
        if hasattr(self, "item"):
            return self.item.aliases.names()
        elif hasattr(self, "property"):
            return self.property.aliases.names()
        else:
            raise NotImplementedError


    class Meta:
        verbose_name_plural = "entities"

class Item(Entity):
    def save(self, *args, **kwargs):
        if not self.slug:
            if Item.objects.count() > 0:
                last_slug = int(Item.objects.last().slug[1:])
                self.slug = "Q" + str(last_slug + 1)
            else:
                self.slug = "Q1"
        super(Item, self).save(*args, **kwargs)


DATATYPE_CHOICES = (
    (1, _("item")),

    (2, _("property")),
    (3, _("string"))
)

class Property(Entity):
    value_type = models.IntegerField(choices=DATATYPE_CHOICES, default=1)

    def save(self, *args, **kwargs):
        if not self.slug:
            if Property.objects.count() > 0:
                last_slug = int(Property.objects.last().slug[1:])
                self.slug = "P" + str(last_slug + 1)
            else:
                self.slug = "P1"
        super(Property, self).save(*args, **kwargs)

    def check_value(self, value):
        if value_type == 1:
            self.check_item(value)
        elif value_type == 2:
            self.check_property(value)
        elif value_type == 3:
            self.check_string(value)

    def check_item(self, value):
        pass

    def check_property(self, value):
        pass

    def check_string(self, value):
        pass


    class Meta:
        verbose_name_plural = "properties"
            

RANK_CHOICES = (
    (1, _("Preferred")),
    (2, _("Normal")),
    (3, _("Deprecated"))
)

class Claim(models.Model):
    entity = models.ForeignKey(Entity, on_delete=models.CASCADE, related_name="claims")
    prop = models.ForeignKey(Property, on_delete=models.CASCADE, related_name="prop_claims")
    value = models.CharField(max_length=200, blank=True)
    rank = models.IntegerField(choices=RANK_CHOICES, default=2)

    def get_value(self):
        value = self.value
        DATATYPE_CHOICES_INDEX = {key: value for key, value in DATATYPE_CHOICES}

        if DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'item':
            return Item.objects.get(slug=value)
        elif DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'property':
            return Property.objects.get(slug=value)
        elif DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'string':
            return value
        return None

    def get_value_display(self):
        result = None
        DATATYPE_CHOICES_INDEX = {key: value for key, value in DATATYPE_CHOICES}

        if DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'item':
            result = Item.objects.get(slug=self.value).label
        elif DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'property':
            result = Property.objects.get(slug=self.value).label
        elif DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'string':
            result = self.value
        return result

    def get_value_url(self):
        result = None
        DATATYPE_CHOICES_INDEX = {key: value for key, value in DATATYPE_CHOICES}

        if DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'item':
            result = Item.objects.get(slug=self.value).get_absolute_url
        elif DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'property':
            result = Property.objects.get(slug=self.value).get_absolute_url
        elif DATATYPE_CHOICES_INDEX[self.prop.value_type] == 'string':
            result = None
        return result

    def get_absolute_url(self):
        return self.entity.get_absolute_url()

    def __str__(self):
        return "{} -> {}".format(self.prop.label, self.value)
