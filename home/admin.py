from django.contrib import admin
from .models import Item, Property, Claim, Entity

# Register your models here.
admin.site.register(Item)
admin.site.register(Property)
admin.site.register(Claim)
