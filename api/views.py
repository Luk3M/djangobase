from django.shortcuts import render
from home.models import Item, Property, Claim
from api.serializers import ItemSerializer, PropertySerializer, ClaimSerializer
from rest_framework import viewsets

class ItemViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                       IsOwnerOrReadOnly,)

     
    def post_save(self, item, *args, **kwargs):
        if type(item.aliases) is list:
            saved_item = Item.objects.get(pk=item.pk)
            for alias in item.aliases:
                saved_item.aliases.add(alias)

class PropertyViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Property.objects.all()
    serializer_class = PropertySerializer
