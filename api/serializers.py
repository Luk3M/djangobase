from rest_framework import serializers
from home.models import Claim, Item, Property, Entity, Claim
from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)

class ClaimSerializer(serializers.ModelSerializer):
    entity = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='slug'
    )

    prop = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='slug'
    )

    class Meta:
        model = Claim
        fields = ('entity', 'prop', 'value', 'rank')

class ItemSerializer(TaggitSerializer, serializers.ModelSerializer):
    aliases = TagListSerializerField()
    claims = ClaimSerializer(many=True)
    url = serializers.CharField(source='get_absolute_url', read_only=True)

    class Meta:
        model = Item
        fields = ('slug', 'label', 'description', 'aliases', 'claims', 'url')

class PropertySerializer(TaggitSerializer, serializers.ModelSerializer):
    aliases = TagListSerializerField()
    claims = ClaimSerializer(many=True)
    url = serializers.CharField(source='get_absolute_url', read_only=True)

    class Meta:
        model = Property
        fields = ('slug', 'label', 'description', 'aliases', 'claims', 'url', 'value_type')
